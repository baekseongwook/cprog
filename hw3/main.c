#include <stdio.h>
#include <ctype.h>

int main(void)
{
    int wordcount = 0; // 총 단어 개수
    int ulwdcount = 0; // 대문자로 시작해 소문자로 끝나는 단어 개수
    char a;
    int i;
    int j;
    i=1;
    j=1;
    a= getchar();
    while(EOF!=a)
    {

        if(i==1)
        {
            if(!isspace(a))
            {
                if(j==1)
                {
                    if(isupper(a))
                    {
                        j=2;
                    }
                }
                ++wordcount;
                i=2;
            }
        }else
        {
            if(j==2 || j==3)
            {
                if(isspace(a))
                {
                    if(j==2)
                    {
                        ++ulwdcount;
                        j=1;
                    }else
                    {
                        j=1;
                    }
                }else
                {
                    if(islower(a))
                    {
                     j=2;
                    }else
                    {
                        j=3;
                    }
                }
            }
            if(isspace(a))
            {
                i=1;
            }
        }
        a=getchar();
    }
    /* ...
    이 부분을 완성하라
       ... */

    printf("%d\n", wordcount); // 전체 단어 개수
    printf("%d\n", ulwdcount); // 첫글자 대문자이고 마지막 글자 소문자인 단어 개수

    return 0;
}
